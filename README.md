**Mosaic Designs by Mosaicist**

At Mosaicist, our designs and color combinations are created to enhance the beauty of mosaic art to any application. The mosaic design capture elegance in a larger form. All of our products are also applicable to floor finishes. Custom Designing by our artists and designers will make your projects one of a kind. Commissions of old world designs to contemporary aquatic motifs are presented before execution Our assistance and installation support help make any floor covering durable and last a lifetime.

*[https://mosaicist.com/](https://mosaicist.com/) *

